const { app, BrowserWindow } = require('electron');
const { spawn } = require('child_process');

function createWindow () {
  // Create the browser window.
  const win = new BrowserWindow({
    width: 1920,
    height: 1080,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  win.loadFile('index.html')

  // Open the DevTools.
  win.webContents.openDevTools()
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.


function record() {
  //Initialize variables
  var inRes = screen.width + "x" + screen.height;
  var fps = 30;

  const recCom = spawn("./startrecording.sh", ["record", inRes, fps]);


  recCom.stdout.on("data", data => {
      console.log(`stdout: ${data}`);
  });

  recCom.stderr.on("data", data => {
      console.log(`stderr: ${data}`);
  });

  recCom.on('error', (error) => {
      console.log(`error: ${error.message}`);
  });

  recCom.on("close", code => {
      console.log(`recCom exited with code ${code}`);
  });

}

function stopReco() {
  const pidKill = spawn("./startrecording.sh", ["stop"]);

  pidKill.stdout.on("data", data => {
      console.log(`stdout: ${data}`);
  });

  pidKill.stderr.on("data", data => {
      console.log(`stderr: ${data}`);
  });

  pidKill.on('error', (error) => {
      console.log(`error: ${error.message}`);
  });

  pidKill.on("close", code => {
      console.log(`pidKill exited with code ${code}`);
  });
}

function play() {
  //Initialize variables
  var inRes = screen.width + "x" + screen.height;
  var fps = 30;

  const playCom = spawn("./startrecording.sh", ["play", inRes, fps]);

  playCom.stdout.on("data", data => {
      console.log(`stdout: ${data}`);
  });

  playCom.stderr.on("data", data => {
      console.log(`stderr: ${data}`);
  });

  playCom.on('error', (error) => {
      console.log(`error: ${error.message}`);
  });

  playCom.on("close", code => {
      console.log(`child process exited with code ${code}`);
  });
}

function homeToSettings() {
  var homeS = document.getElementById("homeScreen");
  var settingS = document.getElementById("settingsScreen");

  homeS.style.display = "none";
  settingS.style.display = "block";
}

function settingsToHome() {
  var homeS = document.getElementById("homeScreen");
  var settingS = document.getElementById("settingsScreen");

  homeS.style.display = "block";
  settingS.style.display = "none";
}
