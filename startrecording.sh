#!/bin/sh
help_fn() {
  echo "Below are examples of how to use this script:"
  echo ""
  echo ""
  echo "To display what you are/will be recording,"
  echo ""
  echo "  ./startrecording.sh play [screen resolution] 30"
  echo ""
  echo "Note that 30 is the frames per second. You can change this."
  echo ""
  echo "To start recording,"
  echo ""
  echo "  ./startrecording.sh record [screen resolution] 30"
  echo ""
  echo "Again, 30 is the fps."
  echo ""
  echo "There will be future updates to this help prompt."
}

play_fn() {
  ffplay -video_size $VAR2 -framerate $VAR3 -f x11grab -i :0.0
}

record_fn() {
  ffmpeg -video_size $VAR2 -framerate $VAR3 -f x11grab -i :0.0 ./videos/currentstreamcodetest.mp4
}

broadcast_fn() {
  echo "not yet implemented."
}

stop_fn() {
  pkill ffmpeg
  echo "Task has been killed."
}

VAR2="$2"
VAR3="$3"

if [ "$1" = "help" ]
then
  help_fn
fi

if [ "$1" = "play" ]
then
  play_fn
fi

if [ "$1" = "record" ]
then
  record_fn
fi

if [ "$1" = "stop" ]
then
  stop_fn
fi

if [ "$1" = "" ]
then
  echo "You failed to provide an argument. For help use \"startrecording.sh help\""
fi
