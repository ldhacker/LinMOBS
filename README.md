LinMOBS is an open source software that we developed to allow people to stream to various platforms on linux. The software is inspired by streamlabs OBS and OBS studio. However, we are not using any of their code or components to accomplish the job. At its most basic level, the sofware is essentially a wrapper for ffmpeg and ffplay. We wanted to take the power of ffmpeg and put it in the user's hands without them needing a lot of technical knowledge to use it.

#### Prerequisites:
- Be on a linux machine (we use and prefer Manjaro Linux [https://manjaro.org/], an arch distro)
- Electron ^10.1.3 (https://www.electronjs.org/)
- Node.js (https://nodejs.org/en/)
- ffmpeg (http://ffmpeg.org/)
- npm (https://www.npmjs.com/)


#### Installing:

1. Download all Prerequisites
2. run the following in terminal:
    - navigate to your desktop
    - git clone https://gitlab.com/ldhacker/LinMOBS.git
    - cd LinMOBS
    - npm install
    - npm start

#### Issues/Errors:
If you encounter any issues or errors while installing or running LinMOBS, please create an issue in this gitlab.
